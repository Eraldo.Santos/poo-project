package poo.project.command.proprietario;

import java.util.Scanner;

import poo.project.command.command;
import poo.project.entities.Proprietario;

public class Update_proprietario implements command {
	
	@Override
	public void execute(Scanner entrada) {
		
		System.out.println("Informe o novo nome do prorpietário:");
		String nome = entrada.next();
		System.out.println("Informe o código do proprietário:");
		int id = entrada.nextInt();	
		Proprietario proprietario = new Proprietario();
		proprietario.update(nome,id);
	}
}
