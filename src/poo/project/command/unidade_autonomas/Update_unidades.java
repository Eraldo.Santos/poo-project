package poo.project.command.unidade_autonomas;

import java.util.Scanner;

import poo.project.command.command;
import poo.project.entities.Unidades;

public class Update_unidades implements command {
	
	public void execute(Scanner entrada) {
		
		System.out.println("Informe o codigo da unidade:");
		int id = entrada.nextInt();
		System.out.println("Informe o codigo do edificio:");
		int numero = entrada.nextInt();
		System.out.println("Informe o codigo do propriet�rio:");
		int id_proprietario = entrada.nextInt();
                System.out.println("Informe o tamanho da unidade:");
		double tamanho = entrada.nextInt();
                String id_inquilino;
                System.out.println("Informe os codigos dos inquilinos no seguinte formato '1,2,3,4':");
		id_inquilino = entrada.next();	
                String[] items = id_inquilino.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
                int[] results = new int[items.length];
                for (int i = 0; i < items.length; i++) {
                    try {
                        results[i] = Integer.parseInt(items[i]);
                    } catch (NumberFormatException nfe) {
                        //NOTE: write something here if you need to recover from formatting errors
                    };
                }
                Unidades unidade = new Unidades();
				unidade.update(id, id_proprietario, results, tamanho);
	}

}
