package poo.project.command.edificio_command;

import java.util.Scanner;

import poo.project.DAO.EdificioDao;
import poo.project.command.command;
import poo.project.entities.Edificio;

public class Update_edificio implements command {
	
	
	
	@Override
	public void execute (Scanner entrada) {
		
		System.out.println("Digite o nome do edificio:");
		String nome = entrada.next();
		System.out.println("Informe o tipo do edificio: Digite 1 para Comercial ou 2 para Resid�ncial");
		int tipo = entrada.nextInt();
		Edificio Edificio = new Edificio();
		System.out.println("Informe o c�digo do edificio:	");
		int id = entrada.nextInt();
		Edificio.update(id, nome, tipo);
		EdificioDao edificioDao = new EdificioDao();
		edificioDao.update(Edificio);	
	}
}
