package poo.project.command.inquilino;

import java.util.Scanner;

import poo.project.command.command;
import poo.project.entities.Inquilino;

public class Update_inquilino implements command {
	@Override
	public void execute(Scanner entrada) {
		
		System.out.println("Informe o novo nome do inquilino:");
		String nome = entrada.next();
		System.out.println("Informe o C�digo do inquilino: ");
		int id = entrada.nextInt();
		Inquilino inquilino = new Inquilino();
		inquilino.update(nome, id);
		
	}
	

}
