package poo.project.command.boleto;

import java.util.Scanner;
import poo.project.DAO.BoletoDao;

import poo.project.command.command;
import poo.project.entities.Boleto;

public class Gerar_boleto implements command {
	
	
	
	@Override
	public void execute (Scanner entrada) {
		
		System.out.println("Digite o valor da despesa do boleto:");
		double despesa = entrada.nextDouble();
		System.out.println("Digite o valor da taxa do condominio do boleto:");
		double taxa = entrada.nextDouble();
        System.out.println("Digite a data de despesa do boleto:");
		String dataDespesa = entrada.next();
        System.out.println("Digite a data de vencimento do boleto:");
		String dataVencimento = entrada.next();
		Boleto boleto = new Boleto(despesa, taxa, dataDespesa, dataVencimento);
		BoletoDao boletoDao = new BoletoDao();
		boletoDao.gerar(boleto);
	}
	
	
	
	

}
