package poo.project.command.boleto;

import java.util.Scanner;
import poo.project.DAO.BoletoDao;

import poo.project.command.command;
import poo.project.entities.Boleto;

public class Recuperar_boleto implements command {
	
	
	
	@Override
	public void execute (Scanner entrada) {
		
        System.out.println("Digite o numero do boleto:");
		int id = entrada.nextInt();
		Boleto boleto = new Boleto(id);
		BoletoDao boletoDao = new BoletoDao();
		boletoDao.getBoleto(boleto);
	}
	
	
	
	

}
