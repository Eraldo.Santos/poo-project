package poo.project.entities;

public class Boleto  {
	
	private double despesas;
	private double taxa;
	private String data_vencimento;
	private String data_despesas;
	private int numero_boleto;
	
	public double getDespesas() {
		return despesas;
	}
	public void setDespesas(double despesas) {
		this.despesas = despesas;
	}
	public double getTaxa() {
		return taxa;
	}
	public void setTaxa(double taxa) {
		this.taxa = taxa;
	}
	public String getData_vencimento() {
		return data_vencimento;
	}
	public void setData_vencimento(String data_vencimento) {
		this.data_vencimento = data_vencimento;
	}
	public String getData_despesas() {
		return data_despesas;
	}
	public void setData_despesas(String data_despesas) {
		this.data_despesas = data_despesas;
	}
	public int getNumero_boleto() {
		return numero_boleto;
	}
	public void setNumero_boleto(int numero_boleto) {
		this.numero_boleto = numero_boleto;
	}
	
	public Boleto() {
		
	}
	
	public Boleto(double despesas, double taxa, String data_despesas, String data_vencimento) {
		
		this.despesas = despesas;
		this.taxa = taxa;
		this.data_despesas = data_despesas;
		this.data_vencimento = data_vencimento;
	}
	
public Boleto(int numero) {
		this.numero_boleto = numero;
	}
	
	
	
	
	

}
