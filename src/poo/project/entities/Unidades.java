package poo.project.entities;

public class Unidades {
	
	private int numero;
	private int id_proprietario;
	private int[] id_inquilino;
	private double tamanho;
	
	public int getNumero() {
		return numero;
	}

	
	public int getId_proprietario() {
		return id_proprietario;
	}

	public void setId_proprietario(int id_proprietario) {
		this.id_proprietario = id_proprietario;
	}


	public int[] getId_inquilino() {
		return id_inquilino;
	}


	public void setId_inquilino(int[] id_inquilino) {
		this.id_inquilino = id_inquilino;
	}

	public double getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}


	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Unidades() {
		
	}
	
	public void update(int numero,int id_proprietario, int[] id_inquilino, double tamanho) {
		
		this.numero = numero;
		this.id_proprietario = id_proprietario;
		this.id_inquilino = id_inquilino;
		this.tamanho = tamanho;
		
	}
	
	public Unidades(int id_proprietario, int[] id_inquilino, double tamanho, int numero) {
		
		this.id_proprietario = id_proprietario;
		this.id_inquilino = id_inquilino;
		this.tamanho = tamanho;
		
	}

	
}
