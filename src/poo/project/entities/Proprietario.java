package poo.project.entities;

public class Proprietario extends Pessoa {
	
	String nome;
	int tipo;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public  Proprietario() {
		
	}

	public void update(String nome, int id) {
		
		super.Update(id);
		this.nome = nome;
		
	}
	
	
	public Proprietario (String nome) {
		
		this.nome = nome;
		this.tipo = 1;
	}
	
}
