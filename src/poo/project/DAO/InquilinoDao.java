/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.project.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import poo.project.entities.Inquilino;


public class InquilinoDao {
	
	public void inserir(Inquilino inquilino) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "insert into inquilino (nome)" +
							" values (?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, inquilino.getNome());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
        
    public void update(Inquilino inquilino) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "update inquilino nome=? where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, inquilino.getNome());
			ps.setInt(3, inquilino.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	 public void remove(Inquilino inquilino) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "delete from inquilino where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, inquilino.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<Inquilino> getInquilino(Inquilino inquilino1) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from inquilino where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, inquilino1.getId());
			ResultSet rs = ps.executeQuery();
			List<Inquilino> inquilinos = new ArrayList<Inquilino>();
			while(rs.next()) {
				Inquilino inquilino = new Inquilino();
				inquilino.setId(rs.getInt(1));
				inquilino.setNome(rs.getString(2));
				inquilinos.add(inquilino);
			}
			return inquilinos;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	public List<Inquilino> getInquilinos() {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from inquilino";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Inquilino> inquilinos = new ArrayList<Inquilino>();
			while(rs.next()) {
				Inquilino inquilino = new Inquilino();
				inquilino.setId(rs.getInt(1));
				inquilino.setNome(rs.getString(2));
				inquilinos.add(inquilino);
			}
			return inquilinos;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}

}
