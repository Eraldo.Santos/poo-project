/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.project.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import poo.project.entities.Boleto;

public class BoletoDao {
	
	public void gerar(Boleto boleto) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "insert into boleto (valor_despesas , taxa_condominio, dt_venc, dt_despesas)" +
							" values (?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, boleto.getDespesas());
            ps.setDouble(2, boleto.getTaxa());
            ps.setString(3, boleto.getData_vencimento());
            ps.setString(4, boleto.getData_despesas());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<Boleto> getBoleto(Boleto boleto1) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from boleto where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, boleto1.getNumero_boleto());
			ResultSet rs = ps.executeQuery();
			List<Boleto> boletos = new ArrayList<Boleto>();
			while(rs.next()) {
				Boleto boleto = new Boleto();
				boleto.setNumero_boleto(rs.getInt(1));
				boleto.setDespesas(rs.getDouble(2));
				boleto.setTaxa(rs.getDouble(3));
                boleto.setData_vencimento(rs.getString(4));
                boleto.setData_despesas(rs.getString(5));
				boletos.add(boleto);
			}
			return boletos;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}

}
