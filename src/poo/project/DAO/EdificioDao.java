/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.project.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import poo.project.entities.Edificio;

public class EdificioDao {
	
	public void inserir(Edificio edificio) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "insert into edificio (nome, tipo)" +
							" values (?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, edificio.getNome());
                        ps.setInt(2, edificio.getTipo());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
        
    public void update(Edificio edificio) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "update edificio nome=?, tipo=? where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, edificio.getNome());
            ps.setInt(2, edificio.getTipo());
			ps.setInt(3, edificio.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	 public void remove(Edificio edificio) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "delete from edificio where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, edificio.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<Edificio> getEdificio(Edificio edificio1) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from edificio where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, edificio1.getId());
			ResultSet rs = ps.executeQuery();
			List<Edificio> edificios = new ArrayList<Edificio>();
			while(rs.next()) {
				Edificio edificio = new Edificio();
				edificio.setId(rs.getInt(1));
				edificio.setNome(rs.getString(2));
				edificio.setTipo(rs.getInt(3));
				edificios.add(edificio);
			}
			return edificios;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	public List<Edificio> getEdificios() {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from edificio";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Edificio> edificios = new ArrayList<Edificio>();
			while(rs.next()) {
				Edificio edificio = new Edificio();
				edificio.setId(rs.getInt(1));
				edificio.setNome(rs.getString(2));
				edificio.setTipo(rs.getInt(3));
				edificios.add(edificio);
			}
			return edificios;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}

}
