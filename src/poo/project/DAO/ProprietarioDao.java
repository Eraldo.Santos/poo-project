/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.project.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import poo.project.entities.Proprietario;


public class ProprietarioDao {
	
	public void inserir(Proprietario proprietario) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "insert into proprietario (nome)" +
							" values (?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, proprietario.getNome());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
        
    public void update(Proprietario proprietario) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "update proprietario nome=? where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, proprietario.getNome());
			ps.setInt(3, proprietario.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	 public void remove(Proprietario proprietario) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "delete from proprietario where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, proprietario.getId());
			ps.executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<Proprietario> getProprietario(Proprietario proprietario1) {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from proprietario where id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, proprietario1.getId());
			ResultSet rs = ps.executeQuery();
			List<Proprietario> proprietarios = new ArrayList<Proprietario>();
			while(rs.next()) {
				Proprietario proprietario = new Proprietario();
				proprietario.setId(rs.getInt(1));
				proprietario.setNome(rs.getString(2));
				proprietarios.add(proprietario);
			}
			return proprietarios;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	public List<Proprietario> getProprietarios() {
		try {
			Connection conn = ConnectionDao.getConnection();
			String sql = "select * from proprietario";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Proprietario> proprietarios = new ArrayList<Proprietario>();
			while(rs.next()) {
				Proprietario proprietario = new Proprietario();
				proprietario.setId(rs.getInt(1));
				proprietario.setNome(rs.getString(2));
				proprietarios.add(proprietario);
			}
			return proprietarios;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}

}
