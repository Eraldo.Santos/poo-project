package poo.project.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDao {
	
    public static Connection getConnection() {
            try {
                    return DriverManager.getConnection(
                                    "jdbc:hsqldb:file:c:/Users/eraldo/Downloads/hsqldb-2.5.0/hsqldb-2.5.0/hsqldb/data/poo","sa","");
            } catch (SQLException e) {
                    throw new RuntimeException(e);
            }
    }

}
