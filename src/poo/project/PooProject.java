/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.project;

import java.util.Scanner;
import poo.project.command.edificio_command.Cadastrar_edificio;

/**
 *
 * @author eraldo
 */
public class PooProject {

    public static void main(String[] args) {
        Cadastrar_edificio cadastrarEdificio = new Cadastrar_edificio();
        Scanner entrada = new Scanner(System.in);
		
        while(true) {
            cadastrarEdificio.execute(entrada);
        }
    }
    
}
